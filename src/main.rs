use read_input::prelude::*;
use std::process;

fn main() {
    println!("What do you want to do? [S]pace, [R]everse [W]ord, [R]everse [L]etters, [E]xit");
    let thing = input::<String>().get();
    match thing.as_ref() {
        "S" => {
            println!("Space");
            space();
        }
        "RW" => {
            println!("Reverse Word");
            reverse_word();
        }
        "RL" => {
            println!("Reverse Letters");
            reverse_letters()
        }
        "E" => {
            println!("Bye!");
            process::exit;
        }
        _ => println!("You didn't enter a valid option!"),
    }
}

fn space() {
    println!("Input what you want to space out:");
    let word = input::<String>().get();
    let wordsplit = word.split("").collect::<Vec<_>>();
    for word in wordsplit {
        print!("{}", word);
        print!(" "); //puts a space between every word
    }
    
    println!("\n"); //makes a new line between spaced words and next line
    //TODO: store the end result
    loop {
        main()
    }
}

fn reverse_word() {
    let reversel = input::<String>().msg("What do you want to reverse:\n").get();
    let mut reverselsplit = Vec::new();
    for item in reversel.split(" ") {
        reverselsplit.push(item);
    }
    let mut x = reverselsplit.len() - 1;
    while x >= 0 {
        print!("{}", reverselsplit[x].to_string());
        print!(" ");
        if x == 0 {
            break;
        } else {
            x = x - 1;
        }
    }
    print!("\n");
    loop {
        main();
    }
}
        
fn reverse_letters() {
    let reversew = input::<String>().msg("What do you want to reverse:\n").get();
    let mut reversewsplit = Vec::new();
    for item in reversew.split("") {
        reversewsplit.push(item);
    }
    let mut x = reversewsplit.len() - 1;
    while x >= 0 {
        print!("{}", reversewsplit[x].to_string());
        if x == 0 {
            break;
        } else {
            x = x - 1;
        }
    }
    print!("\n");
    loop {
        main();
    }
}
